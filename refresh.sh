#!/bin/bash
echo "Project Cleaning Begin"

echo "Cleaning React Cache"
rm -rf $TMPDIR/react-*

echo "Cleaning Metro Cache"
rm -rf $TMPDIR/metro-*

echo "Cleaning Watchman Cache"
watchman watch-del-all

echo "Cleaning Yarn Cache"
yarn cache clean

echo "Removing node_modules"
rm -rf node_modules

echo "Installing node_modules"
yarn install

echo "Removing Existing Pods Folder"
rm -rf ios/Pods

echo "Installing New Pods"
cd ios && pod install && cd ..

echo "Cleaning Android Gradle"
cd android && ./gradlew clean && cd ..

echo "Yarn Refresh Complete"
