Expeneses Tracker app example
===

This is an example Expeneses Tracker app for the demonstration purpose. 

## Table of Contents
---

* [Requirement](#requirement)
* [Installation](#installation)
  * [Cloning from Gitlab](#cloning-from-gitlab)
  * [Running for iOS](#running-for-ios)
  * [Running for Android](#running-for-android)

## Requirement
---
Followings are the **minimum required** build tools to run the project:

* NodeJS 12 or above [Link](https://nodejs.org/en)
* XCode
* Android Studio [Link](https://developer.android.com/studio)

## Installation
---

### Cloning from Gitlab

These are the steps to clone and start building the project.
1. Run this command:
```
git clone https://gitlab.com/jackyhong1994/expensestrackerapp.git
```
2. Go to project root:
```
cd expensestrackerapp
```
3. Install the project:
```
yarn install
```

### Running for iOS

1. Install the dependencies in the podfile.
```
npx pod-install
```
2. Run the app on simulator.
```
npx react-native run-ios
```

### Running for Android

1. Run the app on simulator.
```
npx react-native run-android
```
