import styled from 'styled-components/native'
import React from 'react'
import { SafeAreaView, View } from 'react-native'

export const MainContainer = styled(SafeAreaView)`
  flex:1;
  background-color: white;
`
export const CardContainer = styled(View)`
  border-radius: 8px;
  background-color: #fff;
  box-shadow: 0px 2px 2px #00000050;
  padding: 15px;
  elevation: 2;
  margin-vertical: 10px;
`

export const FormItemContainer = styled.View`
  padding-vertical: 10px;
`