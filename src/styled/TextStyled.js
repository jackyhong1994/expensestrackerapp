import React from 'react'
import styled from 'styled-components/native'

export const RegularText = styled.Text`
  font-family: 'Helvetica';
  font-size: 16px;
`
export const BoldText = styled.Text`
  font-family: 'Helvetica-Bold';
`