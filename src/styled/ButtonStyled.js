import styled from 'styled-components/native'
import React from 'react'
import { TouchableOpacity } from 'react-native'

export const HeaderButton = styled(TouchableOpacity)`
  padding-horizontal: 13px;
`