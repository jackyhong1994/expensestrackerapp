export const CATEGORY = [
  { label: 'Food', value: "Food", color: "#ff000055" },
  { label: 'Shopping', value: "Shopping", color: "#00ff0055" },
  { label: 'Vehicle', value: "Vehicle", color: "#0000ff55" },
  { label: 'Financial expenses', value: "Financial expenses", color: "#ff0fff55" },
  { label: 'Income', value: "Income", color: "#FFBF00" },
]