import { TRANSACTION_PATCH } from './types'

const initialState = {}

export default (state = initialState, action) => {
  const { payload, type } = action

  switch (type) {
    case TRANSACTION_PATCH:
      return {
        ...state,
        ...payload
      }
    default:
      return state
  }
}