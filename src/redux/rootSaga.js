import { all, fork } from 'redux-saga/effects'
import homeSaga from '../modules/home/saga'
import createExpensesSaga from '../modules/create-expenses/saga'
import budgetSaga from '../modules/budget/saga'

function* rootSaga() {
  yield all([
    fork(homeSaga),
    fork(createExpensesSaga),
    fork(budgetSaga)
  ])
}

export default rootSaga