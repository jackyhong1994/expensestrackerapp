import { combineReducers } from 'redux'
import transactionReducer from './app/reducers'
import summaryReducer from '../modules/home/redux/reducers'
import budgetsReducer from '../modules/budget/redux/reducers'

const reducers = {
  summary: summaryReducer,
  transactions: transactionReducer,
  budgets: budgetsReducer
}

const rootReducer = (state, action) => combineReducers({ ...reducers })(state, action)

export default rootReducer