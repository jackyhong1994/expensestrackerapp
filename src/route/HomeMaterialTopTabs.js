import React, { useEffect } from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import Home from '../modules/home/screens/Home'
import Budget from '../modules/budget/screens/Budget'
import { Animated, View, TouchableOpacity, SafeAreaView } from 'react-native';
import HomeHeader from './components/HomeHeader'
import { MainContainer } from '../styled/ContainerStyled'

const { Navigator, Screen } = createMaterialTopTabNavigator()

const HomeMaterialTopTabs = () => {
  return (
    <MainContainer>
      <Navigator
        tabBar={props => <MyTabBar {...props} />}
      >
        <Screen name="Home" component={Home} />
        <Screen name="Budget" component={Budget} />
      </Navigator>
    </MainContainer>
  )
}




const MyTabBar = ({ state, descriptors, navigation, position }) => {
  return (
    <>
      <HomeHeader />
      <View style={{ flexDirection: 'row' }}>
        {state.routes.map((route, index) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
                ? options.title
                : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              // The `merge: true` option makes sure that the params inside the tab screen are preserved
              navigation.navigate({ name: route.name, merge: true });
            }
          };

          const onLongPress = () => {
            navigation.emit({
              type: 'tabLongPress',
              target: route.key,
            });
          };

          const inputRange = state.routes.map((_, i) => i);
          const opacity = position.interpolate({
            inputRange,
            outputRange: inputRange.map(i => (i === index ? 1 : 0)),
          });

          return (
            <TouchableOpacity
              key={label}
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              onLongPress={onLongPress}
              style={{
                flex: 1,
                alignItems: 'center',
              }}
            >
              <Animated.Text style={{ paddingVertical: 10 }}>
                {label}
              </Animated.Text>
              <Animated.View
                style={{
                  width: "100%",
                  height: 2,
                  opacity,
                  backgroundColor: "#710193",
                  shadowColor: '#00000099',
                  shadowOffset: { height: -2 },
                  shadowOpacity: 1,
                  shadowRadius: 4,
                  elevation: 2,
                }} />
            </TouchableOpacity>
          );
        })}
      </View>
    </>
  );
}

export default HomeMaterialTopTabs