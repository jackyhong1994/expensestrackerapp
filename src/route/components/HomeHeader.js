import React, { useEffect, useState } from 'react'
import DropDownPicker from 'react-native-dropdown-picker'
import { useSelector, useDispatch } from 'react-redux'
import { View } from 'react-native'
import { BoldText, RegularText } from '../../styled/TextStyled'

const YEARS = [
  {
    label: new Date().getFullYear() - 1, value: new Date().getFullYear() - 1
  },
  {
    label: new Date().getFullYear(), value: new Date().getFullYear(),
  }
]
const MONTHS = [
  { label: 'January', value: 0 },
  { label: 'February', value: 1 },
  { label: 'March', value: 2 },
  { label: 'April', value: 3 },
  { label: 'May', value: 4 },
  { label: 'June', value: 5 },
  { label: 'July', value: 6 },
  { label: 'Ogos', value: 7 },
  { label: 'September', value: 8 },
  { label: 'October', value: 9 },
  { label: 'November', value: 10 },
  { label: 'December', value: 11 },
]

const TitleWithAmount = ({ title, amount }) => (
  <View style={{ alignItems: 'center' }}>
    <BoldText style={{ color: "#000000" }}>{title}</BoldText>
    <RegularText style={{ color: "#000000" }}>{parseFloat(amount).toFixed(2)}</RegularText>
  </View>
)

const HomeHeader = () => {
  const dispatch = useDispatch()
  const summary = useSelector(state => state.summary)

  const [openMonthPicker, setOpenMonthPicker] = useState(false)
  const [openYearPicker, setOpenYearPicker] = useState(false)
  const [month, setMonth] = useState(new Date().getMonth())
  const [year, setYear] = useState(new Date().getFullYear())

  useEffect(() => {
    dispatch({ type: "GET_SUMMARY", month, year })
  }, [])

  const onChangeMonth = (value) => {
    dispatch({ type: "GET_SUMMARY", month, year })
  }

  const onChangeYear = (value) => {
    dispatch({ type: "GET_SUMMARY", month, year })
  }

  return (
    <View style={{ backgroundColor: 'white' }}>
      <DropDownPicker
        open={openYearPicker}
        value={year}
        items={YEARS}
        setOpen={setOpenYearPicker}
        setValue={setYear}
        listMode="SCROLLVIEW"
        style={{ borderWidth: 0 }}
        zIndex={10000}
        dropDownDirection={'AUTO'}
        textStyle={{
          fontFamily: 'Helvetica',
          fontWeight: 'bold',
          fontSize: 20
        }}
        listItemLabelStyle={{
          fontWeight: 'normal',
          fontSize: 16
        }}
        dropDownContainerStyle={{
          borderWidth: 0,
          shadowColor: '#808080',
          shadowOffset: { width: 3, height: 3 },
          shadowOpacity: 0.2,
          shadowRadius: 2,
          elevation: 2,
        }}
        containerStyle={{
          // flex: 1,
        }}
        onChangeValue={onChangeYear}
      />
      <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
        <DropDownPicker
          open={openMonthPicker}
          value={month}
          items={MONTHS}
          setOpen={setOpenMonthPicker}
          setValue={setMonth}
          textStyle={{
            fontFamily: 'Helvetica',
            fontWeight: 'bold',
            fontSize: 20
          }}
          listItemLabelStyle={{
            fontWeight: 'normal',
            fontSize: 16
          }}
          disableBorderRadius
          style={{ borderWidth: 0 }}
          dropDownDirection={'AUTO'}
          listMode={'MODAL'}
          modalTitle="Pick Month"
          modalTitleStyle={{
            fontFamily: 'Helvetica',
            fontWeight: 'bold'
          }}
          containerStyle={{
            flex: 1,
          }}
          onChangeValue={onChangeMonth}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          paddingVertical: 20
        }}
      >
        <TitleWithAmount title={"Income"} amount={summary.income} />
        <TitleWithAmount title={"Expenses"} amount={summary.expenses} />
        <TitleWithAmount title={"Balance"} amount={summary.balance} />
      </View>
    </View>
  )
}
export default HomeHeader