import React from 'react'
import { createStackNavigator, Header } from '@react-navigation/stack'
import Home from '../modules/home/screens/Home'
import CreateExpenses from '../modules/create-expenses/route'
import EditBudget from '../modules/budget/screens/EditBudget'
import Budget from '../modules/budget/screens/Budget'
import HomeMaterialTopTabs from './HomeMaterialTopTabs'

const MainStack = () => {
  const { Navigator, Screen } = createStackNavigator()

  return (
    <Navigator
      initialRouteName='HomeMaterialTopTabs'
    >
      <Screen name="HomeMaterialTopTabs" component={HomeMaterialTopTabs} options={{ headerShown: false }} />
      <Screen name="CreateExpenses" component={CreateExpenses} options={{ headerTitle: "" }} />
      <Screen name="Budget" component={Budget} options={{ headerTitle: "Budget" }} />
      <Screen name="EditBudget" component={EditBudget} options={{ headerTitle: "" }} />
    </Navigator>
  )
}

export default MainStack