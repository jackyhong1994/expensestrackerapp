import React, { useEffect, useState, useMemo, useCallback } from 'react'
import { Text, FlatList, View, ScrollView, SafeAreaView, Pressable, TouchableOpacity, TouchableHighlight } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { format } from 'date-fns'
import { useNavigation } from '@react-navigation/native'
import DropDownPicker from 'react-native-dropdown-picker'
import { RegularText, BoldText } from '../../../styled/TextStyled'
import { MainContainer, CardContainer } from '../../../styled/ContainerStyled'
import { CATEGORY } from '../../../constants'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

const Home = () => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const summary = useSelector(state => state.summary)

  // const [openMonthPicker, setOpenMonthPicker] = useState(false)
  // const [openYearPicker, setOpenYearPicker] = useState(false)
  // const [month, setMonth] = useState(new Date().getMonth())
  // const [year, setYear] = useState(new Date().getFullYear())


  const onPressAdd = useCallback(() => {
    navigation.navigate("CreateExpenses", { screen: "Expenses", params: { date: new Date(summary.year, summary.month, new Date().getDate()).getTime() } })
  }, [navigation, summary])

  const onPressRow = ({ key, index, type, amount, category }) => {
    const date = new Date(summary.year, summary.month, String(key).split(", ")[0]).getTime()

    if (type === "Expenses") {
      navigation.navigate("CreateExpenses", { screen: "Expenses", params: { date, selectedRowIndex: index, amount, category } })
    } else if (type === "Income") {
      navigation.navigate("CreateExpenses", { screen: "Income", params: { date, selectedRowIndex: index, amount, category } })
    }
  }

  const renderRecords = ([key, value]) => {
    return (
      <CardContainer key={key}>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5 }}>
          <RegularText>{key}</RegularText>
          <RegularText>Expenses: $ {value.totalExpenses}</RegularText>
        </View>
        {
          value?.records.map((item, index) => {
            let symbol = item.type === "Expenses" ? "-" : "+"
            return (
              <TouchableOpacity
                key={index}
                style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5 }}
                onPress={() => { onPressRow({ key, index, type: item.type, amount: item.amount, category: item.category }) }}
              >
                <View
                  style={{ flexDirection: 'row' }}
                >
                  <View style={{ height: "100%", width: 5, marginRight: 5, backgroundColor: CATEGORY.find(cat => cat.label === item.category).color }} />
                  <RegularText>{item.category}</RegularText>
                </View>
                <RegularText>{symbol} {parseFloat(item.amount).toFixed(2)}</RegularText>
              </TouchableOpacity>
            )
          })
        }
      </CardContainer>
    )
  }


  const renderAddRecords = useMemo(() => {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <BoldText
          style={{
            paddingVertical: 50
          }}
        >No record found on this month
        </BoldText>
        <TouchableHighlight
          underlayColor={"#710193"}
          onPress={onPressAdd}
          style={{
            height: 50,
            width: "50%",
            backgroundColor: "#A45EE5",
            borderRadius: 8,
            alignItems: 'center',
            justifyContent: 'center',
            padding: 10
          }}
        >
          <View style={{
            flexDirection: 'row',
            alignItems: 'center',
          }}>
            <MaterialIcon name="plus" size={25} color="white" />
            <BoldText style={{ color: 'white', }}>Add New Record</BoldText>
          </View>

        </TouchableHighlight>
      </View>
    )
  }, [onPressAdd])

  const renderSummaryRecords = useMemo(() => {
    if (summary.dates) {
      return (
        <View>
          {Object.entries(summary.dates).sort((a, b) => b[0].split(", ")[0] - a[0].split(", ")[0]).map(renderRecords)}
        </View>
      )
    } else {
      return (
        renderAddRecords
      )
    }
  }, [summary, renderAddRecords])

  const renderAddButton = useMemo(() => {
    if (summary.dates) {
      return (
        <TouchableHighlight
          underlayColor="#710193"
          onPress={onPressAdd}
          style={{
            position: 'absolute',
            bottom: 20,
            right: 20,
            backgroundColor: '#A45EE5',
            borderRadius: 100,
            width: 50,
            height: 50,
            shadowColor: '#808080',
            shadowOffset: { width: 3, height: 3 },
            shadowOpacity: 0.2,
            shadowRadius: 2,
            elevation: 2,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <MaterialIcon name="plus" size={25} color="white" />
        </TouchableHighlight>
      )
    }

  }, [summary, onPressAdd])


  return (
    <MainContainer>
      {/* {listHeader()} */}
      <ScrollView style={{ flex: 1, backgroundColor: '#00000005' }} contentContainerStyle={{ flexGrow: 1, padding: 13 }}>
        {renderSummaryRecords}
      </ScrollView>
      {renderAddButton}
    </MainContainer>
  )
}

export default React.memo(Home)