import { all, fork } from 'redux-saga/effects'
import {
  watchGetSummary
} from './watchers'

function* homeSaga() {
  yield all([
    fork(watchGetSummary)
  ])
}

export default homeSaga
