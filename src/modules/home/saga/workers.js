import { put, select } from 'redux-saga/effects'
import { SUMMARY_PATCH } from '../redux/types'

export function* showSummaryBy({ year = null, month = null }) {
  try {
    console.log("Getting Summary")
    const transactions = yield select(state => state.transactions)
    const summaryMonth = yield select(state => state.summary.month)
    const currentMonth = month ?? (yield select(state => state.summary.month)) ?? new Date().getMonth()
    const currentYear = year ?? (yield select(state => state.summary.year)) ?? new Date().getFullYear()

    let payload = {}
    let totalExpenses = 0
    let totalIncome = 0
    let dates = {}
    if (transactions && transactions[currentYear] && transactions[currentYear][currentMonth]) {

      for (const key in transactions[currentYear][currentMonth]) {
        let currentDateExpenses = 0
        let currentDateIncome = 0
        transactions[currentYear][currentMonth][key].forEach(item => {
          if (item.type === "Expenses") {
            currentDateExpenses += item.amount
            totalExpenses += item.amount
          }
          if (item.type === "Income") {
            currentDateIncome += item.amount
            totalIncome += item.amount
          }
        })

        dates = {
          ...dates,
          [key]: {
            totalExpenses: currentDateExpenses,
            records: transactions[currentYear][currentMonth][key]
          }
        }
      }

      payload = {
        year: currentYear,
        month: currentMonth,
        income: totalIncome,
        expenses: totalExpenses,
        balance: totalIncome - totalExpenses,
        dates: dates
      }
    } else {
      payload = {
        year: currentYear,
        month: currentMonth,
        income: 0,
        expenses: 0,
        balance: 0,
        dates: null
      }
    }
    console.log("new summary", JSON.stringify({ payload }))
    yield put({ type: SUMMARY_PATCH, payload })
  } catch (error) {
    console.error("showSummaryBy error", error)
  }
}
