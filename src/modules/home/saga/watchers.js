import { takeLatest } from "redux-saga/effects";
import { showSummaryBy } from './workers'

export function* watchGetSummary() {
  yield takeLatest("GET_SUMMARY", showSummaryBy)
}