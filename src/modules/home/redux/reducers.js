
import { SUMMARY_PATCH } from "./types"

const initialState = {
  year: new Date().getFullYear(),
  month: new Date().getMonth(),
  income: 0,
  expenses: 0,
  balance: 0,
  dates: null
}

export default (state = initialState, action) => {
  const { payload, type } = action

  switch (type) {
    case SUMMARY_PATCH:
      return {
        ...state,
        year: payload.year,
        month: payload.month,
        income: payload.income,
        expenses: payload.expenses,
        balance: payload.balance,
        dates: payload.dates
      }
    default:
      return state
  }
}