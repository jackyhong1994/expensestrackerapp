import React, { useState } from 'react'
import { View, Text, TextInput, Alert } from 'react-native'
import { useNavigation, useRoute } from '@react-navigation/native'
import { BoldText, RegularText } from '../../../styled/TextStyled'
import { MainContainer } from '../../../styled/ContainerStyled'
import { useDispatch } from 'react-redux'

const EditBudget = () => {
  const { params } = useRoute()
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const [budgetAmount, setBudgetAmount] = useState("")

  const onChangeBudget = (value) => {
    setBudgetAmount(value)
  }
  const onSubmitEditing = () => {
    if (budgetAmount?.trim().length > 0) {
      dispatch({ type: "ADD_BUDGET", category: params.category, budget: budgetAmount })
      navigation.goBack()
    } else {
      Alert.alert("Invalid Amount", "Please enter amount more than 0")
    }
  }

  return (
    <MainContainer>
      <View style={{ alignItems: 'center', paddingVertical: 20 }}>
        <BoldText style={{ fontSize: 20 }}>Budget Amount</BoldText>
        <TextInput
          keyboardType='number-pad'
          returnKeyType='done'
          onSubmitEditing={onSubmitEditing}
          value={budgetAmount}
          onChangeText={onChangeBudget}
          autoFocus
          style={{
            fontFamily: "Helvetica-Bold",
            fontSize: 40,
            paddingVertical: 20
          }}
        />
      </View>
    </MainContainer>
  )
}

export default EditBudget