import React, { useEffect } from 'react'
import { View, FlatList, TouchableOpacity } from 'react-native'
import { CardContainer, MainContainer } from '../../../styled/ContainerStyled'
import ProgressBar from 'react-native-progress/Bar'
import { CATEGORY } from '../../../constants'
import { BoldText, RegularText } from '../../../styled/TextStyled'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'

const Budget = () => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const budgets = useSelector(state => state.budgets)
  const summary = useSelector(state => state.summary)

  useEffect(() => {
    dispatch({ type: "GET_BUDGET", year: summary.year, month: summary.month })
  }, [summary])

  const onPressRow = ({ category }) => {
    navigation.navigate("EditBudget", { category })
  }

  const renderItem = ({ item, index }) => {
    let expenses = budgets[item]?.[summary.year]?.[summary.month]?.expenses ?? 0.00
    let budget = budgets[item]?.[summary.year]?.[summary.month]?.budget ?? 0
    let percentage = expenses > 0 && budget > 0 ? expenses / budget : 0
    return (
      <CardContainer>
        <TouchableOpacity onPress={() => onPressRow({ category: item })}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
            <BoldText>{item}</BoldText>
            <BoldText style={{ paddingLeft: 10, textAlign: 'center', color: percentage > 1 ? 'red' : "black" }}>{percentage > 1 ? "Overspend" : "Expenses"} {parseInt(expenses)} / {parseInt(budget)} $</BoldText>
          </View>
          <ProgressBar progress={percentage} width={null} style={{ marginTop: 10, flex: 1 }} color={CATEGORY[index].color} showsText />
        </TouchableOpacity>
      </CardContainer>
    )
  }

  return (
    <MainContainer>
      <FlatList
        style={{ backgroundColor: '#00000005' }}
        contentContainerStyle={{ padding: 13, }}
        data={Object.keys(budgets)}
        keyExtractor={item => item}
        renderItem={renderItem}
      />
    </MainContainer>
  )
}

export default React.memo(Budget)