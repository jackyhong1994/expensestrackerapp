import { CATEGORY } from '../../../constants'
import { BUDGET_PATCH, BUDGETS_PATCH } from './types'

let initialState = {}
CATEGORY.forEach(item => {
  initialState = {
    ...initialState,
    [item.value]: null
  }
})

export default (state = initialState, action) => {
  const { payload, type } = action

  switch (type) {
    case BUDGET_PATCH:
      return {
        ...state,
        [payload.category]: {
          ...state[payload.category],
          [payload.year]: {
            ...state[payload.category]?.[payload.year] ?? {},
            [payload.month]: {
              ...state[payload.category]?.[payload.year]?.[payload.month] ?? {},
              budget: payload.budget
            }
          }
        }
      }
    case BUDGETS_PATCH:
      return payload
    default: return state
  }
}