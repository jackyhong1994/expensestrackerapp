import { all, fork } from 'redux-saga/effects'
import { watchAddBudget, watchGetBudget } from './watchers'

function* budgetSaga() {
  yield all([
    fork(watchAddBudget),
    fork(watchGetBudget)
  ])
}

export default budgetSaga