import { takeLatest } from 'redux-saga/effects'
import { addBudget, getBudget } from './workers'

export function* watchAddBudget() {
  yield takeLatest("ADD_BUDGET", addBudget)
}

export function* watchGetBudget() {
  yield takeLatest("GET_BUDGET", getBudget)
}