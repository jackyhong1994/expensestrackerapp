import { put, select } from 'redux-saga/effects'
import { BUDGET_PATCH, BUDGETS_PATCH } from '../redux/types'
import { CATEGORY } from '../../../constants'

export function* getBudget({ year, month }) {
  try {
    console.log("getting budget")
    const summary = yield select(state => state.summary)
    const budgets = yield select(state => state.budgets)
    let records = summary.dates ? Object.values(summary.dates).map(item => item.records).flat() : []

    const reducedRecords = records.reduce((prev, current) => ({
      ...prev,
      [current.category]: {
        ...prev[current.category],
        expenses: (prev[current.category]?.expenses ?? 0) + (current.type === "Expenses" ? current.amount : -current.amount)
      }
    }), {})

    const newBudgets = CATEGORY.reduce((prev, curr) => {
      return ({
        ...prev,
        [curr.value]: {
          ...prev[curr.value],
          [year]: {
            ...(prev[curr.value]?.[year] ?? {}),
            [month]: {
              ...(prev[curr.value]?.[year]?.[month] ?? {}),
              expenses: reducedRecords[curr.value]?.expenses ?? 0
            }
          }
        }
      })
    }, { ...budgets })

    yield put({ type: BUDGETS_PATCH, payload: newBudgets })
  } catch (error) {
    console.error("getBudget error:", error)
  }
}

export function* addBudget({ category, budget }) {
  const summary = yield select(state => state.summary)

  try {
    yield put({ type: BUDGET_PATCH, payload: { budget, category, month: summary.month, year: summary.year } })
  } catch (error) {
    console.error("addBudget error:", error)
  }
}