import React, { useState, useLayoutEffect, useCallback, useMemo, useEffect } from 'react'
import { Text, View, TextInput, Pressable, TouchableOpacity, Alert, ScrollView } from 'react-native'
import { useDispatch } from 'react-redux'
import { useNavigation, useRoute } from '@react-navigation/native'
import DatePicker from 'react-native-date-picker'
import { format } from 'date-fns'
import DropDownPicker from 'react-native-dropdown-picker'
import { BoldText, RegularText } from '../../../styled/TextStyled'
import { MainContainer, FormItemContainer } from '../../../styled/ContainerStyled'
import { HeaderButton } from '../../../styled/ButtonStyled'
import { CATEGORY } from '../../../constants'


const CreateExpenses = ({ type, params, isEdit }) => {
  const dispatch = useDispatch()
  const navigation = useNavigation()
  const [amount, setAmount] = useState(params?.amount ?? "")
  const [date, setDate] = useState((params?.date && new Date(params?.date)) ?? new Date())
  const [openDatePicker, setOpenDatePicker] = useState(false)
  const [openCategoryPicker, setOpenCategoryPicker] = useState(false)
  const [category, setCategory] = useState(params?.category ?? CATEGORY[0].value)
  const [items, setItems] = useState(CATEGORY)

  const headerRight = () => (
    <HeaderButton onPress={onPressSave}>
      <RegularText style={{ fontWeight: '700' }}>{isEdit ? "Save" : "Create"}</RegularText>
    </HeaderButton>
  )

  navigation.getParent().setOptions({
    headerRight
  })

  const onInputAmount = (text) => {
    setAmount(text)
  }

  const onPressDelete = useCallback(() => {
    dispatch({ type: "DELETE_EXPENSES", date, index: params.selectedRowIndex })
    navigation.popToTop()
  }, [navigation, params, date])


  const onPressSave = useCallback(() => {
    let parsedAmount = Number(parseFloat(amount).toFixed(2))
    if (parsedAmount > 0) {
      if (isEdit) {
        dispatch({ type: "UPDATE_EXPENSES", category, amount: Number(parseFloat(parsedAmount).toFixed(2)), date, oriDate: new Date(params.date), expensesType: type, index: params.selectedRowIndex })
      } else {
        dispatch({ type: "SUBMIT_EXPENSES", category, amount: Number(parseFloat(parsedAmount).toFixed(2)), date, expensesType: type, })
      }
      navigation.popToTop()
    } else {
      Alert.alert("Invalid Amount", "Please enter amount more than 0")
    }
  }, [params, category, amount, type, date, isEdit])

  const onPressDateInput = () => {
    console.log("open date picker")
    setOpenDatePicker(true)
  }

  const renderDeleteButton = useMemo(() => {
    if (isEdit) {
      return (
        <TouchableOpacity
          onPress={onPressDelete}
          style={{
            backgroundColor: '#ff000088',
            width: "100%",
            height: 50,
            borderRadius: 8,
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <BoldText style={{ color: 'white' }}>Delete</BoldText>
        </TouchableOpacity>
      )
    }

  }, [isEdit, onPressDelete])

  return (
    <>
      <ScrollView
        style={{ flex: 1 }}
        contentContainerStyle={{ flexGrow: 1 }}
        bounces={false}
      >
        <MainContainer>
          <View style={{ paddingHorizontal: 13 }}>
            <FormItemContainer>
              <RegularText>Amount</RegularText>
              <TextInput keyboardType='number-pad' value={String(amount)} onChangeText={onInputAmount} style={{ paddingVertical: 5, borderBottomWidth: 1, borderBottomColor: "grey" }} />
            </FormItemContainer>
            <FormItemContainer>
              <RegularText>Category</RegularText>
              <DropDownPicker
                open={openCategoryPicker}
                value={category}
                items={items}
                setOpen={setOpenCategoryPicker}
                setValue={setCategory}
                setItems={setItems}
                disableBorderRadius
                style={{ borderWidth: 0 }}
                dropDownDirection={'AUTO'}
                listMode={'MODAL'}
                modalTitle="Choose Category"
                textStyle={{
                  fontFamily: 'Helvetica'
                }}
              />
            </FormItemContainer>
            <FormItemContainer>
              <Pressable onPress={onPressDateInput}>
                <RegularText>Date</RegularText>
                <TextInput editable={false} onPressIn={onPressDateInput} value={format(date, 'd MMM yyyy')} style={{ color: "black", paddingVertical: 5 }} />
              </Pressable>
            </FormItemContainer>
          </View>
          <FormItemContainer style={{ flex: 1, justifyContent: 'flex-end', paddingHorizontal: 13 }}>
            {renderDeleteButton}
          </FormItemContainer>
        </MainContainer>
      </ScrollView>
      <DatePicker
        modal
        mode={'date'}
        open={openDatePicker}
        date={date}
        minimumDate={new Date(new Date().getFullYear() - 1, 0, 1)}
        maximumDate={new Date()}
        onConfirm={(date) => {
          setOpenDatePicker(false)
          setDate(date)
        }}
        onCancel={() => {
          setOpenDatePicker(false)
        }}
      />
    </>
  )
}

export default CreateExpenses