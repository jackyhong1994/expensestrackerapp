import React, { useMemo } from 'react'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { ExpensesForm, IncomeForm } from '../screens'

const { Navigator, Screen } = createMaterialTopTabNavigator()

const MaterialTopRoute = ({ navigation, route }) => {
  const { params } = route?.params ?? {}
  const isEdit = useMemo(() => !!(params?.selectedRowIndex != undefined && params?.selectedRowIndex != null), [params])

  return (
    <Navigator>
      <Screen name="Expenses">
        {props => <ExpensesForm {...props} params={params} isEdit={isEdit} />}
      </Screen>
      <Screen name="Income">
        {props => <IncomeForm {...props} params={params} isEdit={isEdit} />}
      </Screen>
    </Navigator>
  )
}

export default MaterialTopRoute