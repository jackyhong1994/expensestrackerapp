import React, { useEffect, useMemo } from 'react'
import { Alert } from 'react-native'
import CreateExpenses from '../components/CreateExpenses'
import { useNavigation, useRoute } from '@react-navigation/native'

export default ExpensesForm = ({ isEdit, params }) => {
  const navigation = useNavigation()

  const unsubscribe = navigation.addListener('tabPress', (e) => {
    // Prevent default action
    console.log("Expenses", e.target.split("-")[0])
    if (isEdit && e.target.split("-")[0] === "Expenses") {
      e.preventDefault();
      Alert.alert("Record type change", "Do you really want to change record type", [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'Yes',
          onPress: () => {
            navigation.navigate("Expenses", { isEdit, params: params })
          }
        }
      ])
    }
  })

  useEffect(() => () => {
    unsubscribe()
  }, [])

  return (
    <CreateExpenses type="Expenses" params={params} isEdit={isEdit} />
  )
}