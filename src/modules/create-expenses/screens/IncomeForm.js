import React, { useEffect, useMemo } from 'react'
import { Alert } from 'react-native'
import CreateExpenses from '../components/CreateExpenses'
import { useNavigation, useRoute } from '@react-navigation/native'

export default IncomeForm = ({ isEdit, params }) => {
  const navigation = useNavigation()
  // const { params } = useRoute()
  console.log("IncomeForm", params)
  const unsubscribe = navigation.addListener('tabPress', (e) => {
    // Prevent default action
    console.log(e.target.split("-")[0])
    if (isEdit && e.target.split("-")[0] === "Income") {
      e.preventDefault();
      Alert.alert("Record type change", "Do you really want to change record type", [
        {
          text: 'No',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel'
        },
        {
          text: 'Yes',
          onPress: () => {
            navigation.navigate("Income", { isEdit, params: params })

          }
        }
      ])
    }
  })

  useEffect(() => () => {
    unsubscribe()
  }, [])

  return (
    <CreateExpenses type="Income" params={params} isEdit={isEdit} />
  )
}