import { all, fork } from 'redux-saga/effects'
import { watchAddExpenses, watchUpdateExpenses, watchDeleteExpenses } from './watchers'

function* createExpensesSaga() {
  yield all([
    fork(watchAddExpenses),
    fork(watchUpdateExpenses),
    fork(watchDeleteExpenses)
  ])
}

export default createExpensesSaga