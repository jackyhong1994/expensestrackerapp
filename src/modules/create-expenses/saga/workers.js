import { put, select } from 'redux-saga/effects'
import { TRANSACTION_PATCH } from '../../../redux/app/types'
import { format } from 'date-fns'

export function* submitExpenses({ category, amount, date = new Date(), expensesType }) {
  try {
    console.log('Submitting expenses')
    const currentRecords = yield select(state => state.transactions)
    const year = date.getFullYear()
    const month = date.getMonth()
    const formattedDate = format((date), "d, E")
    let newRecords = {
      ...(currentRecords || {}),
      [year]: {
        ...(currentRecords[year] || {}),
        [month]: {
          ...(currentRecords[year]?.[month] || {}),
          [formattedDate]: [
            ...(currentRecords[year]?.[month]?.[formattedDate] || []),
            {
              category,
              amount,
              type: expensesType
            }]
        }
      }
    }
    console.log(JSON.stringify({ newRecords }))
    yield put({ type: TRANSACTION_PATCH, payload: newRecords })
    yield put({ type: "GET_SUMMARY" })
  } catch (error) {
    console.error("submitExpenses error:", error)
  }
}

export function* updateExpenses({ category, amount, date, oriDate, expensesType, index: indexToUpdate }) {
  try {
    console.log('Updating expenses')
    const currentRecords = yield select(state => state.transactions)
    const oriYear = oriDate.getFullYear()
    const oriMonth = oriDate.getMonth()
    const formattedOriDate = format((oriDate), "d, E")
    const year = date.getFullYear()
    const month = date.getMonth()
    const formattedDate = format((date), "d, E")

    //performance can be improved further by processing state in reducer with immerjs or immutablejs.
    let recordToUpdate = { ...currentRecords }
    console.log("indexToUpdate", indexToUpdate)
    recordToUpdate[oriYear][oriMonth][formattedOriDate][indexToUpdate] = {
      category,
      amount,
      type: expensesType
    }

    yield put({ type: TRANSACTION_PATCH, payload: recordToUpdate })
    yield put({ type: "GET_SUMMARY" })
  } catch (error) {
    console.error("updateExpenses error:", error)
  }
}

export function* deleteExpenses({ date, index: indexToUpdate }) {
  try {
    console.log('Deleting expenses')
    const currentRecords = yield select(state => state.transactions)
    const year = date.getFullYear()
    const month = date.getMonth()
    const formattedDate = format((date), "d, E")

    let recordToUpdate = { ...currentRecords }
    recordToUpdate[year][month][formattedDate].splice(indexToUpdate, 1)

    if (recordToUpdate[year][month][formattedDate].length < 1) {
      delete recordToUpdate[year][month][formattedDate]
    }
    if (Object.keys(recordToUpdate[year][month]).length < 1) {
      delete recordToUpdate[year][month]
    }


    yield put({ type: TRANSACTION_PATCH, payload: recordToUpdate })
    yield put({ type: "GET_SUMMARY" })
  } catch (error) {
    console.error("deleteExpenses error:", error)
  }

}