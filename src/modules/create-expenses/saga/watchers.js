import { takeLatest } from 'redux-saga/effects'
import { submitExpenses, updateExpenses, deleteExpenses } from './workers'

export function* watchAddExpenses() {
  yield takeLatest("SUBMIT_EXPENSES", submitExpenses)
}

export function* watchUpdateExpenses() {
  yield takeLatest("UPDATE_EXPENSES", updateExpenses)
}

export function* watchDeleteExpenses() {
  yield takeLatest("DELETE_EXPENSES", deleteExpenses)
}