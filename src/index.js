import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { Text, SafeAreaView } from 'react-native'
import MainStack from './route/MainStack'
import { store, persistor } from './redux/configureStore'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

const App = () => (
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <NavigationContainer>
        <MainStack />
      </NavigationContainer>
    </PersistGate>
  </Provider>
);

export default App
